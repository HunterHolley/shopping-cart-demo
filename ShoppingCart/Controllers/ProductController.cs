﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingCart.Models;
using ShoppingCart.Models.Data;

namespace ShoppingCart.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly DataContext db;

        public ProductController(DataContext context)
        {
            db = context;
        }
        public IEnumerable<Product> Get()
        {
            return db.Products.ToList();
        }

        [Route("{id}")]
        public Product Get(int id)
        {
            return db.Products.Find(id);
        }
    }
}

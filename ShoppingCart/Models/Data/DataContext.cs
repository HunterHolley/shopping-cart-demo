﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart.Models.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options)
            : base(options)
        { }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Product>();

            entity.ToTable("Products");
            entity.HasKey(p => p.Id);
            entity.Property(p => p.Name).HasMaxLength(250).IsRequired();
            entity.Property(p => p.Description).HasMaxLength(1000).IsRequired();
            entity.Property(p => p.Price).HasColumnType("money");


            entity.HasData(new Product[]
            {
                new Product() { Id = 1, Name = "Product 1", Description = "This is the description of Product 1", Price = 1.00M },
                new Product() { Id = 2, Name = "Product 2", Description = "This is the description of Product 2", Price = 2.00M },
                new Product() { Id = 3, Name = "Product 3", Description = "This is the description of Product 3", Price = 3.00M },
                new Product() { Id = 4, Name = "Product 4", Description = "This is the description of Product 4", Price = 4.00M },
                new Product() { Id = 5, Name = "Product 5", Description = "This is the description of Product 5", Price = 5.00M }

            });
        }
    }
}

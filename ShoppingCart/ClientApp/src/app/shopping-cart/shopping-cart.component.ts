import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../services/shopping-cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit { 

  items: Array<any> = [];
  subtotal: number = 0;
  tax: number = 0;
  total: number = 0;
  constructor(private shoppingCartService: ShoppingCartService) { }
    
  ngOnInit() {
    this.items = this.shoppingCartService.getItems();
    this.getTotals();
  }

  onQuantityChange(productId, newQuantity) {
    this.shoppingCartService.updateQuantity(productId, newQuantity);
    this.getTotals();
  }

  onRemove(id: number) {
    this.shoppingCartService.remove(id);
    this.items = this.shoppingCartService.getItems();
    this.getTotals();
  }

  getTotals() {
    this.subtotal = this.shoppingCartService.calculateSubTotal();
    this.tax = this.shoppingCartService.calculateTax();
    this.total = this.shoppingCartService.calculateTotal();
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  private key: string = "shopping-cart-items";
  constructor() { }

  addItem(product: any, quantity: number) {

    let items: Array<any> = this.getItems();
    let alreadyAdded: boolean = false;
    let quantityUpdated: boolean = false;

    for (let i: number = 0; i < items.length; i++) {
      let item = items[i];

      if (item.id == product.id) {
        alreadyAdded = true;

        if (item.quatity != quantity) {
          item.quantity = quantity;  // same item different quantity
          quantityUpdated = true;
        }

        break;
      }

    }

    if (!alreadyAdded) {
      product.quantity = quantity;
      items.push(product);
      this.save(items);
    } else if (quantityUpdated) {
      this.save(items);
    }
  }

  save(items: Array<any>) {
    let json: string = JSON.stringify(items);

    sessionStorage.setItem(this.key, json);
  }

  getItems(): Array<any> {

    let json: string = sessionStorage.getItem(this.key); // retrieve json

    let items: Array<any> = JSON.parse(json || "[]"); // translate to an array of js objects

    return items; //From method
  }

  updateQuantity(productId: number, newQuantity: number) {
    let items = this.getItems();

    for (let i: number = 0; i < items.length; i++) {
      if (items[i].id == productId) {
        items[i].quantity = newQuantity;
        this.save(items);  //updates the sessionStorage
        break;
      }
    }

  }

  remove(id: number) {
    let items = this.getItems();

    for (let i: number = 0; i < items.length; i++) {
      if (items[i].id == id) {
        items.splice(i, 1);
        this.save(items);
        break;
      }
    }
  }
   /*
   My Grocery List:
      •	Eggs (1 dozen)
        o	Qty – 1
        o	Price – 1.29
        o	Total - 1.29
        o	SubTotal – 1.29
      •	Green Beans
        o	Qty – 3 cans
        o	Price – 0.89
        o	Total – 2.67
        o	SubTotal – 3.96
      •	Milk (Gallon)
        o	Qty – 1
        o	Price – 2.50
        o	Total – 2.50
        o	SubTotal – 6.46
      •	Apples
        o	Unit – Pounds
        o	Qty – 2
        o	Price – 1.50
        o	Total – 3.00
        o	SubTotal – 9.46
   */
  calculateSubTotal(): number {
    let items = this.getItems();  //Method getItems stored in variable items

    let subTotal: number = 0;

    if (items && items.length) {
      for (let i: number = 0; i < items.length; i++) {
        let item = items[i];
        let currentItemTotal = item.price * item.quantity;
        subTotal = subTotal + currentItemTotal;
      }

    }
    return subTotal;
  }

  calculateTax(): number {
    let subtotal = this.calculateSubTotal();

    let tax = subtotal * 0.07;

    return tax;
  }

  calculateTotal(): number {
    let subTotal = this.calculateSubTotal();
    let tax = this.calculateTax();

    let total = subTotal + tax;

    return total;
  }
}

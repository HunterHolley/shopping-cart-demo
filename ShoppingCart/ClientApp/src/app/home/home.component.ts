import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { Router } from '@angular/router';

@Component({  
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  products: Array<any> = [];

  constructor(private http: HttpClient, private router: Router, private shoppingCartService: ShoppingCartService, @Inject('BASE_URL') private baseUrl: string) { }

    ngOnInit(): void {
      // throw new Error("Method not implemented.");
      this.http.get(`${this.baseUrl}api/product`).subscribe(response => {
        this.products = response as Array<any>
        });
  }
  addToCart(product: any) {
    this.shoppingCartService.addItem(product, 1);
    this.router.navigate(['shopping-cart']);
  }
}

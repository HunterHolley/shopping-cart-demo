# Shopping Cart Demo
This is a demo app showing basic shopping cart functionality and was created with ASP.Net Core 3.1, EntityFramework Core 3.1.7, and Angular 8.

## Instructions to run the application in your local environment using Visual Studio 2019
1. Right click on the solution and click  the **"Restore NuGet Packages"** option in the context menu to download and install the required packages.
2. Update the connection string value being assigned to the **"ConnectionStrings:DefaultConnection"** property on **"line 11"** in the [appsettings.json file](https://bitbucket.org/HunterHolley/shopping-cart-demo/src/master/ShoppingCart/appsettings.json) in the root of the project with the connection string for the test database server and the name of the database to be created using migration in next step. 
3. Open the **"Package Manager Console" (View > Other Windows > Package Manager Console)** and run the command **"Update-Database"** to create the database.
4. Right click on the **"ClientApp"** folder at root of the project, hover over the **"Open Command Line"** option in the context menu, then choose the command line tool of your preference
5. In the command line tool, run the following commands:
    1. npm i
    2. npm audit fix
    3. npm i @angular-devkit/build-angular@0.803.25 
        * **NOTE**: Due to issues with the VS 2019 Angular Template, package **"@angular-devkit/build-angular"** must be at **version 0.803.25** (Not the latest) in order for the angular app to run in VS 2019 (At least at the time of uploading this code).
        * It is likely you will see vunerabilities after this command complete, but not many. Do not run **"npm audit fix"** or it will upgrade the **"@angular-devkit/build-angular"** package to it's latest version.
        * If you wish to view the vunerabilities run the **"npm audit"** command. This will allow you to address each vunerabity individually without affecting the verson of the **"@angular-devkit/build-angular"** package
        * Due to the fact that this is a demo, I would reccomend not worrying about fixing any vunerabilities that appear after running the 3rd command, unless one of the vunerabilites prevent you from running the app.
6. Finally press **"F5"** to start the application.
7. Enjoy!!!!!!!